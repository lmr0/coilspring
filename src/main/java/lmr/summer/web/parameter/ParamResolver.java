package lmr.summer.web.parameter;


import lmr.summer.urlparam.CParam;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpMethod;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.reactive.BindingContext;
import org.springframework.web.reactive.result.method.HandlerMethodArgumentResolver;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

public class ParamResolver implements HandlerMethodArgumentResolver {

    GetParamResolver gpr = new GetParamResolver();
    PostParamResolver prr = new PostParamResolver();

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterAnnotation(Param.class) != null;
    }

    @Override
    public Mono<Object> resolveArgument(MethodParameter parameter, BindingContext bindingContext, ServerWebExchange exchange) {
        ServerHttpRequest request = exchange.getRequest();

        if(request.getMethod() == HttpMethod.POST){
            return prr.resolveArgument(parameter,bindingContext,exchange);
        }
        if(request.getMethod() == HttpMethod.GET){
            return gpr.resolveArgument(parameter,bindingContext,exchange);
        }
        CParam param = new CParam();

        return Mono.just(param);
    }
}