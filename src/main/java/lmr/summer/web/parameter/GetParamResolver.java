package lmr.summer.web.parameter;


import lmr.summer.SessionManager;
import lmr.summer.cmap.CMap;
import lmr.summer.urlparam.CParam;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpCookie;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.BindingContext;
import org.springframework.web.reactive.result.method.HandlerMethodArgumentResolver;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Set;

public class GetParamResolver implements HandlerMethodArgumentResolver {


    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return (parameter.getParameterAnnotation(GetParam.class) != null) || (parameter.getParameterAnnotation(GetLoginParam.class) != null);
    }

    @Override
    public Mono<Object> resolveArgument(MethodParameter parameter, BindingContext bindingContext, ServerWebExchange exchange) {

        ServerHttpRequest request = exchange.getRequest();

        String accesstoken = "";
        try {
            accesstoken = exchange.getRequest().getHeaders().getFirst("Authorization");
        }catch(Exception e){}
        if(accesstoken==null) accesstoken = "";

        CParam param = new CParam();
        CParam cookie = new CParam();

        CMap user = new CMap();

        MultiValueMap cookiemap = request.getCookies();
        Set<String> cookiemapkeys = cookiemap.keySet();



        for(String key :cookiemapkeys){
            List<HttpCookie> paramValues = (List<HttpCookie>) cookiemap.get(key);

            HttpCookie result = null;
            if (paramValues != null) {
                result = (paramValues.size() == 1 ? paramValues.get(0) : paramValues.get(0));
            }
            cookie.put(key,result.getValue());
        }

        try {
            if("".equals(accesstoken)){
                accesstoken = cookie.getS("sessionid");
            }else {
                accesstoken = accesstoken.substring(6).trim();
            }

            CMap suser = SessionManager.getInstance().getSessionMap(accesstoken);

            //  if(suser==null || !suser.eqS("accountkind","ADMIN")) throw new RouteBusinessException("notlogin");

            user.putAll(suser);

        }catch(Exception e){
          //  throw new RouteBusinessException("notlogin");
        }

        try{
            param.put("loginaccountpkey",user.getS("accountpkey","-1"));
            param.put("sitepkey",user.getS("sitepkey","-1"));
        }catch(Exception e){}


        MultiValueMap map = request.getQueryParams();

        Set<String> keys = map.keySet();

        for(String key :keys){
            List<String> paramValues = (List<String>) map.get(key);

            Object result = null;
            if (paramValues != null) {
                result = (paramValues.size() == 1 ? paramValues.get(0) : paramValues);
            }
            param.put(key,result);
        }

        param.put("aseatoken", accesstoken);
        param.put("user",user);


        return Mono.just(param);
    }
}