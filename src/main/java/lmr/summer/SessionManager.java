package lmr.summer;




import lmr.summer.cmap.CMap;

import java.util.Base64;
import java.util.UUID;

public class SessionManager {
    private static SessionManager instance = new SessionManager();


    CMap session = new CMap();

    private SessionManager(){

    }
    public static SessionManager getInstance(){

        return instance;
    }


    public String makeSessionKey(){
        String key1 = UUID.randomUUID().toString();
        String key = Base64.getEncoder().encodeToString(key1.getBytes());
        this.session.put(key,new CMap());
        return key;
    }

    public CMap getSessionMap(String key){
        CMap ret = (CMap) this.session.get(key);
        return ret;
    }

    public boolean expireSessionKey(String key){
        this.session.remove(key);
        return true;
    }


}
