package lmr.summer.urlparam;


import lmr.summer.cmap.CMap;

import java.util.Map;



public class CParam extends CMap {
    CMap head = new CMap();

    public CParam(){

    }

    public void validateEmpty(String... keys) throws CParamValidateException{
        for (String key: keys) {
            if(!isEmpty(key)){
                throw new CParamValidateException(key);
            }
        }

    }




    public void setByRequestParam(Map r){
        this.putAll(r);
        return;
    }




    public boolean isEmpty(String key) {
        String aa = this.getS(key,"0-12judhioewd789iohfug5tbh4o3itghf");

        if( "0-12judhioewd789iohfug5tbh4o3itghf".equals(aa)) return true;
        if("".equals(aa.trim())) return true;

        return false;
    }



    public class CParamValidateException extends RuntimeException {
        String problem = "";
        public CParamValidateException(String key) {
            this.problem = key;
        }

        @Override
        public String getMessage() {
            return this.problem;
        }
    }
}
