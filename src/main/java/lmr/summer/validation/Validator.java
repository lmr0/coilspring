package lmr.summer.validation;

public class Validator {
    public static <T> T checkNull(T a,String msg){
        if(a==null){
            throw new RuntimeException(msg);
        }
        return a;
    }

    public static <T> T validate(T a,String msg){
        if(a==null){
            throw new RuntimeException(msg);
        }
        return a;
    }
}
