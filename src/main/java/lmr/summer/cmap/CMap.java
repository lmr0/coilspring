package lmr.summer.cmap;

import java.util.*;

public class CMap implements Map<String,Object>,SummerDO{
    Map<String,Object> map;


    public CMap(Map<String,Object> baseMap) {
        this.map = baseMap;
    }


    public CMap(){
        this(new HashMap<String,Object>());
    }

    public CMap(String... key){
        this();

        for(int i=0;i<key.length;i=i+2) {
            this.put(key[i],key[i+1]);
        }

    }



    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return map.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return map.containsValue(value);
    }

    @Override
    public Object get(Object key) {

        return map.get(key);
    }


    @Override
    public Object put(String key, Object value) {
        return map.put(key,value);
    }


    @Override
    public Object remove(Object key) {
        return map.remove(key);
    }

    @Override
    public void putAll(Map m) {
        map.putAll(m);
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public Set keySet() {
        return map.keySet();
    }

    @Override
    public Collection values() {
        return map.values();
    }

    @Override
    public Set<Entry<String, Object>> entrySet() {
        return map.entrySet();
    }

    public String getS(String key){
        return getS(key,"");
    }
    public String getS(String key,String defaultValue){
        return this.map.getOrDefault(key,defaultValue).toString();
    }

    public boolean eqS(String key,String v){ String a=getS(key); if(a==null || !a.equals(v)){ return false;} return true;}

    public int getI(String key, int i){
        try {
            return Integer.parseInt(this.getS(key), 10);
        }catch(Exception e){
            return i;
        }
    }

    public Object getOne(String a){
        Object rr = this.get(a);
        if( rr instanceof List){
            List aa = (List) rr;
            return aa.get(0);
        }else{
            return rr;
        }
    }

    public double getD(String key, double doubleValue){
        try {
            return Double.parseDouble(this.getS(key));
        }catch(Exception e){
            return doubleValue;
        }
    }

    public long getL(String key,long longValue){
        try {
            return Long.parseLong(this.getS(key),10);
        }catch(Exception e){
            return longValue;
        }

    }

    public List<CMap> getList(String key) { try{return (List<CMap>)this.get(key);}catch(Exception e){return new ArrayList<CMap>();}}

    @Override
    public String toString() {
        return super.toString();
    }

    public CMap clean(String... keys) {
        CMap ret = new CMap();
        for(String key : keys) {
            ret.put(key,get(key));
        }
        return ret;
    }

    public CMap remove(String... keys) {
        CMap ret = new CMap();
        for(String key : keys) {
            ret.remove(key);
        }
        return ret;
    }

    @Override
    public Class getType() {
        return CMap.class;
    }
}
