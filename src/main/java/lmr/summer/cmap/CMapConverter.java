package lmr.summer.cmap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 *
 * @author lmr
 */


public class CMapConverter {
    static ObjectMapper mapper = new ObjectMapper();
    public static CMap makeCMap(String json) throws JsonProcessingException{
        CMap ret = new CMap();
        makeCMap(ret, json);
        return ret;
    }

    public static  List<CMap> makeCMapList(String json)  throws JsonProcessingException{
        List<CMap> ret = new ArrayList<CMap>();
        makeCMapList(ret, json);
        return ret;
    }


    public static String toJson(Object map){

        try {
            return mapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            return "can not serialize. "+map;
        }

    }

    public static void makeCMap(CMap ret, String json)  throws JsonProcessingException{
        ObjectMapper jsonMapper = new ObjectMapper();
        JsonNode node = jsonMapper.readTree(json);

        if(node.isArray()){
            List<CMap> vlist = new ArrayList<>();
            ret.put("list",vlist);
            makeCMapList(vlist,node);
            return;
        }else if(node.isContainerNode()){
            makeCMap(ret,node);
            return;
        }

    }


    public static void makeCMapList(List<CMap> ret,String json)  throws JsonProcessingException{
        ObjectMapper jsonMapper = new ObjectMapper();
        JsonNode node = jsonMapper.readTree(json);

        if(node.isArray()){
            makeCMapList(ret, node);
            return;
        }else if(node.isContainerNode()){
            CMap vret = new CMap();
            ret.add(vret);
            makeCMap(vret,node);
            return;
        }

    }
    public static void makeCMapList(List<CMap>  ret, JsonNode node) {
        if(node.isArray()){
            for(int i = 0;i<node.size();i++){
                CMap cm = new CMap();
                ret.add(cm);

                makeCMap(cm, node.get(i));
            }
        }


    }

    public static void makeCMap(CMap ret, JsonNode json) {
        Iterator<String> keys = json.fieldNames();
        while (keys.hasNext()) {
            String key = (String) keys.next();

            JsonNode val = json.get(key);

            if(val.isArray()){
                List<CMap> list = new ArrayList<CMap>();
                ret.put(key, list);
                makeCMapList(list,val);
            }else if(val.isContainerNode()){
                CMap vret = new CMap();
                ret.put(key,vret);
                makeCMap(vret,val);
            }else{
                ret.put(key,val.asText(""));
            }
        }
    }

}
